package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

/**
 * 
 * A CellAutomata that implements Conways game of life.
 * 
 * @see CellAutomaton
 * 
 *      Every cell has two states: Alive or Dead. Each step the state of each
 *      cell is decided from its neighbors (diagonal, horizontal and lateral).
 *      If the cell has less than two alive Neighbors or more than three
 *      neighbors the cell dies. If a dead cell has exactly three neighbors it
 *      will become alive.
 * 
 * @author eivind
 * @author Martin Vatshelle - martin.vatshelle@uib.no
 * @author Sondre Bolland - sondre.bolland@uib.no
 */
public class GameOfLife implements CellAutomaton {

	/**
	 * The grid of cells
	 */
	IGrid currentGeneration;

	/**
	 * 
	 * Construct a Game Of Life Cell Automaton that holds cells in a grid of the
	 * provided size
	 * 
	 * @param height The height of the grid of cells
	 * @param width  The width of the grid of cells
	 */
	public GameOfLife(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		return currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		return currentGeneration.numColumns();
	}

	@Override
	public CellState getCellState(int row, int col) {
		return currentGeneration.get(row, col);
	}

	@Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		
		for (int row=0; row<nextGeneration.numRows(); row++) {
    		for (int col=0; col<nextGeneration.numColumns(); col++) { 
    			nextGeneration.set(row,col,getNextCell(row,col));
    		}
		}

	}

	@Override
	public CellState getNextCell(int row, int col) {
		CellState state = null;
		if ((getLivingNeighbours(row, col) == 2 ) || (getLivingNeighbours(row, col) == 3))  {
			state = CellState.ALIVE;
		} else if ((getLivingNeighbours(row, col) < 2 ) || (getLivingNeighbours(row, col) > 3)){
			state = CellState.DEAD;
		}
		return state;

	}

	private int getLivingNeighbours(int row, int col) {
		int neighbourCount = 0;
		int rowStart = row-1;
		int colStart = col-1;
		int rowEnd = row+1;
		int colEnd = col+1;
		if (getCellState(row, col) == CellState.ALIVE) {
			neighbourCount = -1;
		}
		
		if (row == 0) {
			rowStart = row;
		}
		if (col == 0) {
			colStart = col;
		}
		if (row == numberOfRows()-1) {
			rowEnd = row;
		}
		if (col == numberOfColumns()-1) {
			colEnd = col;

		}
		for (int rows=rowStart; rows<=rowEnd; rows++) {
			for (int columns=colStart; columns<=colEnd; columns++) {
				if (getCellState(rows,columns).equals(CellState.ALIVE)) {
					neighbourCount++;			
				}
			}
		}
		return neighbourCount;

	}

	/**
	 * Calculates the number of neighbors having a given CellState of a cell on
	 * position (row, col) on the board
	 * 
	 * Note that a cell has 8 neighbors in total, of which any number between 0 and
	 * 8 can be the given CellState. The exception are cells along the boarders of
	 * the board: these cells have anywhere between 3 neighbors (in the case of a
	 * corner-cell) and 5 neighbors in total.
	 * 
	 * @param x     the x-position of the cell
	 * @param y     the y-position of the cell
	 * @param state the Cellstate we want to count occurences of.
	 * @return the number of neighbors with given state
	 */
	//private int countNeighbors(int row, int col, CellState state) {
	//	return 0;
	//}

	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}
}

package datastructure;

import java.util.ArrayList;

import cellular.CellState;

public class CellGrid implements IGrid {
	
	private int rows;
	private int columns;
	private CellState[][] cells;

    public CellGrid(int rows, int columns, CellState initialState) {
		this.rows = rows;
		this.columns = columns;
		this.cells = new CellState[rows][columns];
		for (int row=0; row<rows; row++) {
			for(int col=0; col<columns; col++) {
				cells[row][col] = initialState;
				
			}
		}
	}

    @Override
    public int numRows() {
        return this.rows;
    }

    @Override
    public int numColumns() {
        return this.columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
    	try {
    		this.cells[row][column] = element;
    	}
        catch (IllegalArgumentException e) {
        	throw e;
        }
        
    }

    //private int indexOf(int row, int column) {
	//	return row*column;
	//}

	@Override
    public CellState get(int row, int column) {
		try {
			return this.cells[row][column];
		}
        catch (IllegalArgumentException e) {
        	throw e;
        }
    }

    @Override
    public IGrid copy() {
        CellGrid gridCopy = new CellGrid(rows, columns, null);
        for(int row=0; row<rows; row++) {
        	for(int col=0; col<columns; col++) {
        		gridCopy.set(row, col, this.get(row, col));
        	}
        }
		// TODO Auto-generated method stub
        return gridCopy;
    }
    
}

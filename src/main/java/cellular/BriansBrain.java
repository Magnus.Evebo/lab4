package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton {
	IGrid currentGeneration;
	Random random = new Random();

	
	public BriansBrain(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}



	@Override
	public CellState getCellState(int row, int column) {
		return currentGeneration.get(row, column);
	}
	@Override
	public int numberOfRows() {
		return currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		return currentGeneration.numColumns();
	}

	@Override
	public void initializeCells() {
		for (int row = 0; row < numberOfRows(); row++) {
			for (int col = 0; col < numberOfColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}


	}

	@Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		for (int row=0; row<nextGeneration.numRows(); row++) {
    		for (int col=0; col<nextGeneration.numColumns(); col++) { 
    			nextGeneration.set(row,col,getNextCell(row,col));
    		}
		}
		currentGeneration = nextGeneration;


	}

	@Override
	/////////
	public CellState getNextCell(int row, int col) {
		CellState state = null;
		if (getCellState(row,col).equals(CellState.DEAD)) {
			if (getLivingNeighbours(row, col) == 2) {
				state = CellState.ALIVE;	
			} else {
				state = CellState.DEAD;
			}
		}
		
		else if (getCellState(row,col).equals(CellState.DYING)) {
			state = CellState.DEAD;
		} 
		else {
			state = CellState.DYING;
		}
	
		return state;

	}
	
	//kopi fra GameOfLife
	private int getLivingNeighbours(int row, int col) {
		int neighbourCount = 0;
		int rowStart = row-1;
		int colStart = col-1;
		int rowEnd = row+1;
		int colEnd = col+1;
		if (getCellState(row, col) == CellState.ALIVE) {
			neighbourCount = -1;
		}
		
		if (row == 0) {
			rowStart = row;
		}
		if (col == 0) {
			colStart = col;
		}
		if (row == numberOfRows()-1) {
			rowEnd = row;
		}
		if (col == numberOfColumns()-1) {
			colEnd = col;

		}
		for (int rows=rowStart; rows<=rowEnd; rows++) {
			for (int columns=colStart; columns<=colEnd; columns++) {
				if (getCellState(rows,columns).equals(CellState.ALIVE)) {
					neighbourCount++;			
				}
			}
		}
		return neighbourCount;
	}



	@Override
	public IGrid getGrid() {
		// TODO Auto-generated method stub
		return currentGeneration;
	}

}
